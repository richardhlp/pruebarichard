import { Pipe, PipeTransform } from '@angular/core';
import { isDate } from 'util';


@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {
edad=0;
  transform(value: string,year:number): any {

    
    if(value!=null && value!=''){
    let arr=value.split('-')
    
    let convertDate = new Date();
    convertDate.setFullYear(parseInt(arr[2]));
    convertDate.setMonth(parseInt(arr[1]));
    convertDate.setDate(parseInt(arr[0]));
     
     const timeDiff = Math.abs(Date.now() - convertDate.getTime());
     this.edad= Math.floor((timeDiff / (1000 * 3600 * 24))/365);
     
    }
    else if(year>0){
      
      const a = new Date();
      
      this.edad=a.getFullYear() - year;
    }
    if (this.edad>0)
    return this.edad;
   else return "";
  }

}
