import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import{map} from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class HogwartsService {
  public casas:string[]=
  [
     "slytherin",
     "gryffindor",
     "ravenclaw",
     "hufflepuff"
    ];
  baseAdrr='http://hp-api.herokuapp.com/api/characters';

  constructor(private httpClient: HttpClient) { 
    console.log("Servicio listo");
  }
setHeaders(){
  
}

getPersonajes(casa:string){
 
return this.httpClient.get(`${this.baseAdrr}/house/${casa}`);

}

getProfesores(){
 
return this.httpClient.get(`${this.baseAdrr}/staff`);

}
getEstudiantes(){
 
return this.httpClient.get(`${this.baseAdrr}/students`);

}
}
