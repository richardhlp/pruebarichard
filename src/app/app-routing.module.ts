import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PersonajesComponent } from './components/personajes/personajes.component';
import { EstudiantesComponent } from './components/estudiantes/estudiantes.component';
import { ProfesoresComponent } from './components/profesores/profesores.component';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';

const routes: Routes = [
  {path:'home',component:HomeComponent},
  {path:'personajes',component:PersonajesComponent},
  {path:'estudiantes',component:EstudiantesComponent},
  {path:'profesores',component:ProfesoresComponent},
  {path:'solicitudes',component:SolicitudesComponent},
  {path:'**' ,redirectTo:'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
