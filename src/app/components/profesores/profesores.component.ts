import { Component, OnInit } from '@angular/core';
import { HogwartsService } from '../../services/hogwarts.service';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.css']
})
export class ProfesoresComponent implements OnInit {
  profesores:any=[];
  cuadricula=false;
  loading=false;
  loaded=false;
  constructor(private service: HogwartsService) {
    this.listarProfesores();
   }

  ngOnInit(): void {
  }

  listarProfesores(){
    localStorage.setItem("cuadricula",this.cuadricula.toString());
  this.loading=true;
  this.service.getProfesores().subscribe((profesores:any[])=>{
      this.profesores=profesores;
      console.log(this.profesores);
      this.loading=false
      
      if(profesores.length>0){
        this.loaded=true;
      }else{
        this.loaded=false;
      }
    });

  }
}
