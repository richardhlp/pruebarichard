import { Component, OnInit } from '@angular/core';
import { HogwartsService } from '../../services/hogwarts.service';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {
  estudiantes:any=[];
  cuadricula=false;
  loading=false;
  loaded=false;
  constructor(private service:HogwartsService) {
    this.listarEstudiantes();
   }

  ngOnInit(): void {
  }

  listarEstudiantes(){
    localStorage.setItem("cuadricula",this.cuadricula.toString());
    this.loading=true;
    this.service.getEstudiantes().subscribe((estudiantes:any[])=>{
        this.estudiantes=estudiantes;
        console.log(this.estudiantes);
        this.loading=false
        
        if(estudiantes.length>0){
          this.loaded=true;
        }else{
          this.loaded=false;
        }
      });
  
    }

}
