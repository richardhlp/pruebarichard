import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Estudiante } from '../../interfaces/estudiante';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {
public estudiante:Estudiante={};
public estudiantes:Estudiante[]=[];
  constructor() {
    if(localStorage.getItem('Estudiantes')){

      let est=localStorage.getItem('Estudiantes');
      console.log(est);
      this.estudiantes=JSON.parse(est);
    }
   }

  ngOnInit(): void {
  }

  guardar(forma:NgForm){

   if(forma.valid){
     this.estudiante.name=forma.form.value['name'];
     this.estudiante.patronus=forma.form.value['patronus'];
     this.estudiante.dateOfBirth=forma.form.value['dateOfBirth'];
     console.log(this.estudiante);
     
     this.estudiantes.push(this.estudiante);
     localStorage.setItem('Estudiantes',JSON.stringify( this.estudiantes));
    }
      
      
  }

}
