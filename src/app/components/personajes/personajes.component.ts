import { Component, OnInit } from '@angular/core';
import { HogwartsService } from '../../services/hogwarts.service';
import{Observable,Operator} from 'rxjs'


@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent implements OnInit {
  personajes:any=[];
  casas:string[];
  cuadricula=false;
  loading=false;
  loaded=false;
  constructor(private service: HogwartsService) { }

  ngOnInit(): void {

    //Obtengo la lista de casas desde el servicio
    this.casas=this.service.casas;

  }

  listarPersonajes(casa:string){
    if(casa==""){
      this.personajes=null;
      this.loaded=false;
      return;
    }
  this.loading=true;
  localStorage.setItem("cuadricula",this.cuadricula.toString());
    
    this.service.getPersonajes(casa).subscribe((personajes:any[])=>{
      this.personajes=personajes;
      console.log(this.personajes);
      this.loading=false;
      
  if(personajes.length>0){
    this.loaded=true;
  }else{
    this.loaded=false;
  }
    });

  }

  }

