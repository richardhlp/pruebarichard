import { Component, OnInit,Input } from '@angular/core';


@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
cuadricula:boolean=true;
  @Input() items: any[]=[];
  @Input() loaded: boolean;
 
  newItems:any[]=[];
  constructor() { 
    if(localStorage.getItem("cuadricula")){
      var c=localStorage.getItem("cuadricula");
      this.cuadricula= JSON.parse(c.toLowerCase());
      
    }
      
  }

  ngOnInit(): void {

  }

  buscar(texto:string){
   
    this.actualizarVista();
    if(texto==="")
    {
      location.reload();
    }
    else
    {
      
      for(let item of this.items)
      {
      
        let nombre= item.name.toLowerCase();
       
        if(nombre.toString().toLowerCase().indexOf(texto.toLowerCase())>=0)
        {
          this.newItems.push(item);
        }
      
        this.items=this.newItems;
      }
    }

}
actualizarVista(){
  this.cuadricula=!this.cuadricula;
  console.log("actualizar");
  
  localStorage.setItem("cuadricula",this.cuadricula.toString());
}

}
