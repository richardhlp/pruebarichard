import { NgModule,LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PersonajesComponent } from './components/personajes/personajes.component';
import { EstudiantesComponent } from './components/estudiantes/estudiantes.component';
import { ProfesoresComponent } from './components/profesores/profesores.component';
import{ HttpClientModule} from '@angular/common/http';
import { AgePipe } from './pipes/age.pipe';
import { LoadingComponent } from './components/loading/loading.component';
import {registerLocaleData} from '@angular/common';
import localEs from '@angular/common/locales/es';
import { ItemsComponent } from './components/items/items.component';
import { BusquedaComponent } from './components/busqueda/busqueda.component';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';
import { FormsModule } from '@angular/forms';

registerLocaleData (localEs);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    NavbarComponent,
    PersonajesComponent,
    EstudiantesComponent,
    ProfesoresComponent,
    AgePipe,
    LoadingComponent,
    ItemsComponent,
    BusquedaComponent,
    SolicitudesComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide:LOCALE_ID,
      useValue:'es'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
